import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class HeadersInterceptorService implements  HttpInterceptor {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler){
    req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    console.log(req)
    return next.handle(req)
  }
  
}



