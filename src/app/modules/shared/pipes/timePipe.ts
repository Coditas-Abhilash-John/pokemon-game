import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'time'})
export class TimePipe extends DatePipe implements PipeTransform {
  override transform(value: any, args?: any): any {
    let date = new Date(value)
    return super.transform(date, "h:mm a");
  }
}
