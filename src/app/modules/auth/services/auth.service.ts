import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUserLoginCredentials } from '../models/userLoginInterface';
import { AuthModule } from '../auth.module';
import { environment  } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}


  post(endPoint: string, data: IUserLoginCredentials) {
    return this.httpClient.post(`${environment.baseUrl}${endPoint}`, data);
  }

  logUser(userLoginData: IUserLoginCredentials): Observable<any> {
    return this.post('auth/login', userLoginData);
  }
}
