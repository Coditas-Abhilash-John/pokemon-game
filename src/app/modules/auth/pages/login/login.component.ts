import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { IUserLoginCredentials } from 'src/app/modules/auth/models/userLoginInterface';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLogin: boolean = true;
  constructor(private authService: AuthService, private router : Router) {}
  url ='https://images.unsplash.com/photo-1647892591880-58c55fd726d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80';

  ngOnInit(): void {}
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
  });

  userLogin(userLoginData: IUserLoginCredentials) {
    this.authService.logUser(userLoginData).subscribe((response) => {
      if (response.statusCode === 200 && response.data.token)
        window.localStorage.setItem('token', response.data.token);
        this.router.navigate(['/pokemon'])
    });
  }

  onLogin(userLoginForm: FormGroup) {
    if (userLoginForm.valid) {
      this.userLogin({
        email: userLoginForm.value.email,
        password: userLoginForm.value.password,
      });
    }
  }

  // isValid(formControlName: FormControlName) {
  //   return (
  //     formControlName.hasError(formControlName.name + '') &&
  //     !formControlName.hasError('required')
  //   );
  // }
  // isEmpty(formControlName: FormControl) {
  //   return formControlName.hasError('required') && formControlName.dirty;
  // }
}
