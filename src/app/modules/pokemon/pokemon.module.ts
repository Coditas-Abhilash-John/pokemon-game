import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonRoutingModule } from './pokemon-routing.module';
import { PokemonComponent } from './pokemon.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SharedModule } from '../shared/shared.module';
import { PokeTableComponent } from './features/poke-table/poke-table.component';
import {MatTableModule} from '@angular/material/table';
import { PokemonGuardService } from './services/pokemon-guard.service';
import { DialogComponent } from './features/dialog/dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import { TypePipe } from './pipes/typePipe';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    PokemonComponent,
    HomePageComponent,
    PokeTableComponent,
    DialogComponent,
    TypePipe
  ],
  imports: [
    CommonModule,
    PokemonRoutingModule,
    SharedModule,
    MatTableModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDialogModule,
    MatPaginatorModule  ],
  providers:[PokemonGuardService]
})
export class PokemonModule { }
