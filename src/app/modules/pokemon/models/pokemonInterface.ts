export interface IPokemonData {
  id?: string,
  name: string,
  level: number,
  type: number,
  isSelected?: boolean,
  createdOn?: string,
  updatedOn?: string,
}
export interface IDialogData {
  dialogTitle: string,
  buttonType: string,
  pokemonData ?: IPokemonData
}
