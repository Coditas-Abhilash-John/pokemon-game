import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { IPokemonData } from '../../models/pokemonInterface';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PokemonService } from '../../services/pokemon.service';
import { AlertDialogComponent } from 'src/app/modules/shared/components/alert-dialog/alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-poke-table',
  templateUrl: './poke-table.component.html',
  styleUrls: ['./poke-table.component.scss'],
})
export class PokeTableComponent implements OnInit {
  pokemonList: IPokemonData[] = [];
  dataSource!: MatTableDataSource<IPokemonData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  displayedColumns: string[] = [
    'name',
    'level',
    'type',
    'createdOn',
    'updatedOn',
    'update',
    'delete',
  ];

  getPokemonRecords() {
    this.pokemonService.getPokemon().subscribe((value) => {
      this.pokemonList = value.data;

      this.dataSource = new MatTableDataSource<IPokemonData>(
        Object(value).data
      );
      this.dataSource.paginator = this.paginator;
    });
  }

  constructor(
    private pokemonService: PokemonService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getPokemonRecords();
  }
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();

  onDelete(id: string) {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '300px',
      data: { dialogTitle: 'Delete Pokemon', dialogContent: 'Are you sure ?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.pokemonService.deletePokemon(id).subscribe(() => {
          this.getPokemonRecords();
        });
      }
    });
  }

  updatePokemon(pokemonData: IPokemonData, result: IPokemonData) {
    this.pokemonService
      .updatePokemon({
        ...pokemonData,
        ...result,
      })
      .subscribe((response) => {
        console.log(response);
      });
    this.getPokemonRecords();
  }

  onUpdate(pokemonData: IPokemonData): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: {
        dialogTitle: 'Update Pokemon',
        buttonType: 'Update',
        pokemonData,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.updatePokemon(pokemonData, result);
    });
  }
  createPokemon(pokemonData: IPokemonData) {
    pokemonData = {
      ...pokemonData,
      isSelected: true,
    };
    this.pokemonService.createPokemon(pokemonData).subscribe((response) => {
      console.log(response);
      this.getPokemonRecords();
    });
  }

  onCreate() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: { dialogTitle: 'Create Pokemon', buttonType: 'Create' },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.createPokemon(result);
      }
    });
  }
}
