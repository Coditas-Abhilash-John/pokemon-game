import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IDialogData, IPokemonData } from '../../models/pokemonInterface';

const types = [
  {
    id: 1,
    name: 'Water',
  },
  {
    id: 2,
    name: 'Fire',
  },
  {
    id: 3,
    name: 'Leaf',
  },
  {
    id: 4,
    name: 'Steel',
  },
];

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  pokemonTypes = types;
  pokemonData: IPokemonData = {
    name: '',
    level: 0,
    type: 1,
  };
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: IDialogData
  ) {}

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    level: new FormControl(0, [Validators.required]),
    type: new FormControl(1, [Validators.required]),
  });

  onSubmit(customForm: FormGroup) {
    if (customForm.valid) {
      this.dialogRef.close(customForm.value);
    }
  }
  ngOnInit(): void {
    this.pokemonData = this.dialogData.pokemonData || this.pokemonData;
    this.form.setValue({
      name: this.pokemonData.name,
      level: this.pokemonData.level,
      type: this.pokemonData.type
    });
  }
}
