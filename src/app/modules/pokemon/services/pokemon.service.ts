import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, retry } from 'rxjs';
import { environment  } from 'src/environments/environment';
import { IUserLoginCredentials } from '../../auth/models/userLoginInterface';
import { IPokemonData } from '../models/pokemonInterface';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) {}

  httpHeader = {
    headers: new HttpHeaders({
      'Authorization' : this.getToken() || "",
    }),
  };

  get(){
    return this.httpClient.get(`${environment.baseUrl}pokemon`,this.httpHeader)
  }
  post( data: IPokemonData) {
    return this.httpClient.post(`${environment.baseUrl}pokemon`, data,this.httpHeader);
  }
  delete( id : string) {
    return this.httpClient.delete(`${environment.baseUrl}pokemon/${id}`,this.httpHeader);
  }
  update(data: IPokemonData) {
    return this.httpClient.put(`${environment.baseUrl}pokemon`, data,this.httpHeader);
  }


  getPokemon(): Observable<any>{
    return this.get()
  }
  deletePokemon(id:string){
    return this.delete(id)
  }
  updatePokemon(data:IPokemonData){
    return this.update(data)
  }
  createPokemon(data:IPokemonData){
    return this.post(data)
  }

  getToken(){
    return window.localStorage.getItem("token")
  }


}
