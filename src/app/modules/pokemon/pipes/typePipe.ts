import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'type'})
export class TypePipe implements PipeTransform {


  transform(value: any, ...args: any[]) {
    const types = [{
      id: 1,
      name: 'Water'
    }, {
      id: 2,
      name: 'Fire'
    }, {
      id: 3,
      name: 'Leaf'
    }, {
      id: 4,
      name: 'Steel'
    }]
    return types.find(type =>
      type.id === value
    )?.name
  }

}
