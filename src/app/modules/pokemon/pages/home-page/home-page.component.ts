import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { DialogComponent } from '../../features/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { IPokemonData } from '../../models/pokemonInterface';
import { Router } from '@angular/router';
import { AlertDialogComponent } from 'src/app/modules/shared/components/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private router: Router
  ) {}


  ngOnInit(): void {
  }

  onLogout() {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '300px',
      data: { dialogTitle: 'Logout', dialogContent: 'Are you sure ?' },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        window.localStorage.removeItem('token');
        this.router.navigate(['/auth']);
      }
    });
  }
}
